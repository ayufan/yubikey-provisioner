# Yubikey provisioner

This tool does try to provision and disable unused applets of YubiKey.

It configures:

- NFC/USB interface
- Disable OTP and OATH
- GPG
- PIV
- FIDO2
- Configures PIN, Admin PIN, PUK and MGMT KEY
- Requires PIN (for the first time), Touch (always) for each operation

## Configure one and many Yubikeys

> - It wipes a configuration of GPG/PIV/FIDO2
> - Disables OTP and OATH applet
> - These changes are revertible to factory defaults

Just run `./configure-yubikey` and follow instructions.

You will be asked for Password. This password is used to encrypt
GPG keys.

See `public/` to see the available SSH keys and GPG key.
Consider uploading your GPG public key to
https://keyserver1.pgp.com/vkd/GetUploadKeyScreen.event

### Key Formats

The `configure-yubikey` does support two key formats:

- `RSA` (default) - all SSH keys are configured with 2048 bits (except master key using 4096),
   as this does offer the best compatibility. These keys are also imported in `PIV` applet

- `ED25519` (optional) - this uses a new ECC Curve25519 cipher. This requires a Yubico with
   at least 5.2.3 firmware. These keys can only be used in `OpenGPG` as `PIV` does not support it

Configure the subkeys format with:

- `KEY_FORMAT=rsa ./configure-yubikey` (default)
- `KEY_FORMAT=ed ./configure-yubikey`

## Secure your data

You can use GPG, and PIV.

It is advised to store a strongly encrypted `keys/` folder
using offline storage. Likely printed on paper and put in a bank box :)
If you want to print it use a scan friendly OCR font [OCR-A](https://en.wikipedia.org/wiki/OCR-A).

It is advised to run this tool using some Live Linux, so the `keys/` is never
persisted to disk, rather it is stored in memory only.

## Homebrew Tap

After configuring and securing your data. You can start using them.

The easiest is to install Homebrew Tap that will configure GPG/PIV usage on Mac OS:

```bash
brew install ayufan/taps/ssh-with-gpg
brew services start ayufan/taps/ssh-with-gpg
brew cask install yubico-yubikey-manager yubico-yubikey-piv-manager
```

This will provide a `ssh-with-gpg` and `sshpiv` scripts:

- `ssh-with-gpg` does configure `SSH_AUTH_SOCK` to use GPG and watch for GPG notification
- `sshpiv` does force the usage of `PIV` applet allowing to access additional keys configured

## GPG

It creates a master key and collection of three subkeys (A, C, S).

Currently, it uses RSA keys, as the A, C, S keys are also imported into PIV applet.

### Manually configure GPG use for SSH (for Mac)

Install: `brew install gnupg2 pinentry-mac`

To later use GPG auth for SSH do:

```bash
cat <<EOF > ~/.gnupg/gpg-agent.conf
enable-ssh-support
pinentry-program /usr/local/bin/pinentry-mac
EOF
```

```bash
cat <<EOF >> ~/.zshrc # or ~/.bashrc
gpgconf --launch gpg-agent
ln -sf $HOME/.gnupg/S.gpg-agent.ssh $SSH_AUTH_SOCK
EOF
```

Start a new terminal session and check if a new key did appear:

```bash
ssh-add -L
```

Additionally, you can import older keys with `ssh-add` into `gnupg2`
keyring.

### Manually configure GPG use for SSH (for Linux) [alpha]

Install: `apt-get install gnupg2 pinentry-gnome3`

To later use GPG auth for SSH do:

```bash
cat <<EOF > ~/.gnupg/gpg-agent.conf
enable-ssh-support
EOF
```

Configure `Gnome` to autostart GPG in SSH mode:

```bash
mkdir -p ~/.config/autostart
cat <<EOF > ~/.config/autostart/gnome-keyring-ssh.desktop
[Desktop Entry]
Type=Application
Name=SSH Key Agent
Comment=GNOME Keyring: SSH Agent
Exec=/usr/bin/gnome-keyring-daemon --start --components=ssh
OnlyShowIn=GNOME;Unity;MATE;
X-GNOME-Autostart-enabled=false
X-GNOME-Autostart-Phase=PreDisplayServer
X-GNOME-AutoRestart=false
X-GNOME-Autostart-Notify=true
X-GNOME-Bugzilla-Bugzilla=GNOME
X-GNOME-Bugzilla-Product=gnome-keyring
X-GNOME-Bugzilla-Component=general
X-GNOME-Bugzilla-Version=3.34.0
X-Ubuntu-Gettext-Domain=gnome-keyring
EOF
```

Re-login and start a new terminal session and check if a new key did appear:

```bash
ssh-add -L
```

Additionally, you can import older keys with `ssh-add` into `gnupg2`
keyring.

## PIV

It uses subkeys private keys to generate certificates and to prefil PIV applet.

It additionally imports `keys/id_*` into additional PIV slots,
allowing to use older keys in a special circumstances.

You can use PIV applet instead of `gnupg2` for SSH auth. For that purpose install:
`brew install yubico-piv-tool` with `ykcs11` lib.

### Manually configure PIV for SSH

To use PIV stored SSH keys, you can use it on-demand:

```bash
# add it to current ssh-agent session
eval $(ssh-agent -P "/*")
ssh-add -s /usr/local/lib/libykcs11.dylib

# use the PIV for a specific connection only
ssh -I /usr/local/lib/libykcs11.dylib git@gitlab.com
```

It can be helpful to simply have these aliases in `.zshrc` or `.bash_profile`:

```bash
alias sshagentpiv='eval $(ssh-agent -P "/*"); killall gpg-agent; ssh-add -s /usr/local/lib/libykcs11.1.dylib'
alias sshpiv='ssh -I /usr/local/lib/libykcs11.dylib'
```

## Author

Kamil Trzcinśki, MIT, 2020
